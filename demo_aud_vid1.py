# PWM on GPIO28 for left, GPIO27 for right.

from time import sleep
from machine import Pin,PWM

audio_left = Pin(28, Pin.OUT)
audio_right = Pin(27, Pin.OUT)

pwm_audio_left = PWM(audio_left)
pwm_audio_right = PWM(audio_right)

def beep2(freq,time):
    pwm_audio_left.freq(freq)
    pwm_audio_right.freq(freq)
    pwm_audio_left.duty_u16(32768)
    pwm_audio_right.duty_u16(32768)
    sleep(time)
    pwm_audio_left.deinit()
    pwm_audio_right.deinit()

# First interface, just play a frequency and a time.
def play(freq, ms_time):
  beep2(freq, ms_time/1000)

play(122,350)
play(522,250)

sleep(0.3)


for i in range(5):
    play(440+40*i,100)

